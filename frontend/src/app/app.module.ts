import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { httpInterceptorProviders } from './auth/auth-interceptor';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { MultiSelectModule } from 'primeng/multiselect';
import { SliderModule } from 'primeng/slider';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MessagesModule } from './components/messages/messages';
import { MessageModule } from './components/message/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { HomeComponent } from './pages/home/home.component';
import { SidebarModule } from 'primeng/sidebar';
import { SidebarmenuComponent } from './components/sidebarmenu/sidebarmenu.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import {InputMaskModule} from 'primeng/inputmask';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SidebarmenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    TableModule,
    HttpClientModule,
    InputTextModule,
    DialogModule,
    ButtonModule,
    MultiSelectModule,
    SliderModule,
    DropdownModule,
    RouterModule,
    AutoCompleteModule,
    MessagesModule,
    MessageModule,
    ConfirmDialogModule,
    SidebarModule,
    BreadcrumbModule,
    InputMaskModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
