import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CampaignRequest } from '../domain/requests/CampaignRequest';
import { CampaignsDto } from '../domain/dtos/Campaigns';

const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token',
    'Access-Control-Allow-Origin': environment.apiUrl,
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
    'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
});
const httpOptions = {
    headers
};
@Injectable()
export class CampaignService {

    constructor(private http: HttpClient) { }
    private _baseUrl = environment.apiUrl + "/campaigns";

    getAllCampaigns(): Observable<any> {
        return this.http.get<any[]>(this._baseUrl + "/getAll", { headers })
            .pipe();
    }

    getAllCampaignsCustom(campaignRequest: CampaignRequest, sortVar: any): Observable<any> {
        let params = new HttpParams();
        params = params.append('page', campaignRequest.page.toString());
        params = params.append('size', campaignRequest.size.toString());
        if (sortVar != "")
            params = params.append('sort', sortVar);

        return this.http.post<any[]>(this._baseUrl + "/getAllCustom",
            campaignRequest.filters, { headers, params })
            .pipe()
    }


    getAllCampaignsBySearchTerm(campaignRequest: CampaignRequest,
         sortVar: string, searchTerm: string): Observable<any> {
        let params = new HttpParams();
        params = params.append('page', campaignRequest.page.toString());
        params = params.append('size', campaignRequest.size.toString());
        if (sortVar != "")
            params = params.append('sort', sortVar);

        return this.http.post<any[]>(this._baseUrl + "/getAllBySearchTerm",
            searchTerm, { headers, params })
            .pipe()
    }


    saveCampaign(campaignDto: CampaignsDto): Observable<any> {
        return this.http.post<any[]>(this._baseUrl + "/saveCampaign", campaignDto, { headers })
            .pipe()
    }

    getAllEndDates() {
        return this.http.get<any[]>(this._baseUrl + "/getAllEndDates")
            .toPromise()
            .then(data => data);
    }

    getStatusList() {
        return this.http.get<any[]>(this._baseUrl + "/getStatusList")
            .toPromise()
            .then(data => data);
    }
    private handleError(err: HttpErrorResponse) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage = '';
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return Observable.throw(errorMessage);
    }
}
