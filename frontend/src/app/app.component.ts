import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './auth/token-storage.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { MessageService } from 'primeng/components/common/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class AppComponent implements OnInit {
  info: any;

  constructor(private tokenStorage: TokenStorageService,
    private router: Router, private confirmationService: ConfirmationService,
    private messageService: MessageService) { }

  ngOnInit() {

    this.info = {
      token: this.tokenStorage.getToken(),
      username: this.tokenStorage.getUsername()
    };
  }

  logout() {
    this.tokenStorage.signOut();
    this.router.navigate(['/auth/login']);
    window.location.reload();
  }
}
