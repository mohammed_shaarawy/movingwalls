import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { SidebarmenuComponent } from './components/sidebarmenu/sidebarmenu.component';
// import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes), RouterModule.forChild([
      { path: '', component: SidebarmenuComponent }])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
// { path: '', component: BreadcrumbComponent }