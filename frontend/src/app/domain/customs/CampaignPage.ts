import { CampaignCustom } from './CampaignCustom';

export class CampaignPage {
    constructor(public campaignCustomList?: Array<CampaignCustom>,
        public totalPagesCount?: number,
        public totalRecordsCount?: number) { }
}