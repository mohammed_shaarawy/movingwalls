export class CampaignCustom{
    id: any;
    name: string;
    status: string;
    startDate: Date;
    endDate: Date;
    duration: string;
    daysCount: any;    
}