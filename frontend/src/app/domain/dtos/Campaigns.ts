export class CampaignsDto {
    constructor(public id?: number, public name?: string, public status?: string,
        public startDate?: Date, public endDate?: Date) { }
}
