export class CampaignRequest {
    constructor(public page: number = 0,
        public size: number = 10, public filters?: any) { }
}