import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from '../../auth/token-storage.service';
import { CampaignService } from '../../services/CampaignService';
import { LazyLoadEvent, MenuItem, SelectItem, ConfirmationService, MessageService, Message } from 'primeng/components/common/api';
import { CampaignRequest } from '../../domain/requests/CampaignRequest';
import { CampaignPage } from '../../domain/customs/CampaignPage';
import { CampaignsDto } from 'src/app/domain/dtos/Campaigns';
import { GlobalSearchService } from 'src/app/services/GlobalSearchService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [CampaignService]
})
export class HomeComponent implements OnInit {
  info: any;
  title: string;
  cols: any[];
  headercols: any[];
  campaignPage: CampaignPage = new CampaignPage();
  totalRecords: number;
  sortVar: string = "";
  campaignRequest: CampaignRequest = new CampaignRequest();
  items: MenuItem[];
  home: MenuItem;
  listEndDates: SelectItem[];
  listStatus: SelectItem[];
  displayDialog: Boolean = false;
  campaignDto: CampaignsDto = new CampaignsDto();
  loading: boolean;
  errorMessage: string;
  statusSuggestions: string[];
  msgs: Message[] = [];
  @Input() searchTerm: string = "";
  selectedCampaigns: any[];

  constructor(private router: Router, private compaignService: CampaignService,
    private token: TokenStorageService, private confirmationService: ConfirmationService,
    private messageService: MessageService, private globalSearchService: GlobalSearchService) { }


  ngOnChanges() {
    this.compaignService.getAllCampaignsBySearchTerm(this.campaignRequest, this.sortVar,
      this.searchTerm).subscribe((data) => {
        this.campaignPage = data;
        this.totalRecords = data.totalRecordsCount;
        console.log("campaigns data: " + JSON.stringify(this.campaignPage))
      }, ((error) => {
      }));
  }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
    };

    if (!this.info.token)
      this.router.navigate(['/auth/login']);

    this.cols = [
      { field: 'name', header: 'Campaign' },
      { field: 'startDate', header: 'Duration' },
      { field: 'status', header: 'Status' },
      { field: 'daysCount', header: 'Report' }
    ];

    this.headercols = [
      { field: 'name', header: 'Campaign', width: '25%' },
      { field: 'duration', header: 'Duration', width: '25%' },
      { field: 'status', header: 'Status', width: '15%' },
      { field: 'daysCount', header: 'Report', width: '25%' }
    ];
    this.title = 'Campaigns';

    this.items = [
      { label: 'Campaigns' }
    ];
    this.home = { icon: 'pi pi-home', routerLink: '/' };
    this.loadDropDownFilters();
    this.initiateFiltersList();
  }

  loadCampaignsTableLazy(event: LazyLoadEvent) {
    this.mapFilterDropdownValues(event);
    this.setSortAndPagination(event);
    this.compaignService.getAllCampaignsCustom(this.campaignRequest, this.sortVar).subscribe((data) => {
      this.campaignPage = data;
      this.campaignPage.totalPagesCount = data.totalPagesCount;
      this.campaignPage.totalRecordsCount = data.totalRecordsCount;
      this.totalRecords = data.totalRecordsCount;
      console.log("campaigns data: " + JSON.stringify(this.campaignPage))
    }, ((error) => {
    }));
  }

  initiateFiltersList() {
    this.campaignRequest.filters = {
      "endDate": [],
      "status": []
    };
  }

  setSortAndPagination(event: LazyLoadEvent) {
    if (event.sortField) {
      if (event.sortField === "name")
        this.sortVar = "name"
      else if (event.sortField === "status")
        this.sortVar = "status"

      if (event.sortOrder === 1)
        this.sortVar += "," + "asc"
      else
        this.sortVar += "," + "desc"
    }

    console.log("number of rows first page: " +  event.first);
    console.log("number of rows: " + event.rows);
    console.log("size of page: " + event.rows);
    this.campaignRequest.page = event.first / event.rows;
    this.campaignRequest.size = event.rows;
  }


  loadDropDownFilters() {
    this.compaignService.getAllEndDates().then((data) => {
      this.listEndDates = [];
      for (let i = 0; i < data.length; i++) {
        this.listEndDates.push({ label: data[i], value: data[i] });
      }
    });

    this.compaignService.getStatusList().then((data) => {
      this.listStatus = [];
      for (let i = 0; i < data.length; i++) {
        this.listStatus.push({ label: data[i], value: data[i] });
      }
    });
  }


  mapFilterDropdownValues(event: LazyLoadEvent) {
    console.log("filters: " + event.filters);
    this.campaignRequest.filters.endDate = [];
    this.campaignRequest.filters.status = [];
    if (event.filters && JSON.stringify(event.filters) != "{}") {
      if (event.filters.endDate) {
        console.log("filter endDate toCreate : " + event.filters.endDate);

        for (let i = 0; i < event.filters.endDate.value.length; ++i)
          this.campaignRequest.filters.endDate[i] = event.filters.endDate.value[i];
      }
      else {
        console.log("filter endDate toDelete : " + event.filters.endDate);
        delete this.campaignRequest.filters.endDate;
      }

      if (event.filters.status) {
        console.log("filter status toCreate : " + event.filters.status);

        for (let i = 0; i < event.filters.status.value.length; ++i)
          this.campaignRequest.filters.status[i] = event.filters.status.value[i];
      }
      else {
        console.log("filter status toDelete : " + event.filters.status);
        delete this.campaignRequest.filters.status;
      }

    }
  }

  showDialogToAdd() {
    this.displayDialog = true;
  }

  saveCampaign(event: LazyLoadEvent) {
    console.log(JSON.stringify(this.campaignDto));
    this.compaignService.saveCampaign(this.campaignDto).subscribe((data) => {
      // debugger;
      console.log("Data returned after call save/update :" + JSON.stringify(data));
      console.log("event filters in save method :" + JSON.stringify(event));
      this.loadDropDownFilters();
      this.executeSearch();
    }, ((error) => {
      this.errorMessage = error;
      console.log("Error returned after call save/update :" + error)
    }));
    this.displayDialog = false;
    this.cleanDialog();
  }

  confirmCancel() {
    this.confirmationService.confirm({
      message: 'Are you sure to cancel?',
      header: 'Cancel Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.displayDialog = false;
      },
      reject: () => {
        this.messageService.add({ severity: 'info', summary: 'Rejected', detail: 'You have cancelled!' });
        this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have cancelled!' }];
      }
    });
  }
  executeSearch() {
    this.loading = true;
    this.compaignService.getAllCampaignsCustom(this.campaignRequest,
      this.sortVar).subscribe((data) => {
        // debugger;
        this.campaignPage = data;
        this.totalRecords = data.totalRecordsCount;
        this.loading = false;
      }, ((error) => {
        this.errorMessage = error;
      }));
  }

  cleanDialog() {
    this.campaignDto.id = null;
    this.campaignDto.name = "";
    this.campaignDto.status = "";
    this.campaignDto.startDate = new Date();
    this.campaignDto.endDate = new Date();
  }

  public getAllCampaignsBySearchTerm(event: any) {
    // this.setSortAndPagination(event);
    this.compaignService.getAllCampaignsBySearchTerm(this.campaignRequest, this.sortVar,
      event.target.value).subscribe((data) => {
        this.campaignPage = data;
        this.totalRecords = data.totalRecordsCount;
        console.log("campaigns data: " + JSON.stringify(this.campaignPage))
      }, ((error) => {
      }));
  }



  //   $(document).ready(function () {

  //     $('#sidebarCollapse').on('click', function () {
  //         $('#sidebar').toggleClass('active');
  //     });

  // });

  selectStudent(event, student) {
  }

}
