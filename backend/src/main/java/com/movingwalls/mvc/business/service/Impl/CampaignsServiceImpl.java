package com.movingwalls.mvc.business.service.Impl;

import com.movingwalls.mvc.business.domain.customs.CampaignCustom;
import com.movingwalls.mvc.business.domain.customs.CampaignPage;
import com.movingwalls.mvc.business.domain.dtos.CampaignFiltersDto;
import com.movingwalls.mvc.business.domain.dtos.CampaignsDto;
import com.movingwalls.mvc.business.domain.enums.Status;
import com.movingwalls.mvc.business.service.interfaces.CampaignsServiceInterface;
import com.movingwalls.mvc.dao.entities.Campaigns;
import com.movingwalls.mvc.dao.repository.CampaignsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import sun.util.resources.cldr.hy.CalendarData_hy_AM;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * @author Muhammad
 * @since 7/31/2020.
 */

@Service
public class CampaignsServiceImpl implements CampaignsServiceInterface {

    @Autowired
    private CampaignsRepository campaignsRepository;

    @Override
    public List<CampaignsDto> getAllCampaigns() {
        List<Campaigns> campaignsList = campaignsRepository.findAll();
        return this.mapCampaignsListToDtoList(campaignsList);
    }


    @Override
    public CampaignPage getAllCampaignCustoms(CampaignFiltersDto campaignFiltersDto, Integer page,
                                              Integer size, Sort sort) {
        Pageable pageable = PageRequest.of(page, size, sort);
        Page<Campaigns> campaignsPage = this.findCampaignPage(campaignFiltersDto, pageable);
        List<CampaignCustom> campaignCustomList = this.mapCampaignPageToCampaignCustomList(campaignsPage);
        return new CampaignPage(campaignCustomList, campaignsPage.getTotalPages(),
                campaignsPage.getNumberOfElements());
    }

    private List<CampaignCustom> mapCampaignPageToCampaignCustomList(Page<Campaigns> campaignsPage) {
        List<Campaigns> campaignsList = campaignsPage.getContent();
        List<CampaignsDto> campaignsDtoList = this.mapCampaignsListToDtoList(campaignsList);
        return this.calculateDaysForAllCampaigns(campaignsDtoList);
    }

    @Override
    public CampaignPage findAllBySearchTerm(String searchTerm, Integer page, Integer size, Sort sort) {
        Pageable pageable = PageRequest.of(page, size, sort);
        Page<Campaigns> campaignsPage = campaignsRepository.findAll(searchTerm, pageable);
        List<CampaignCustom> campaignCustomList = this.mapCampaignPageToCampaignCustomList(campaignsPage);
        return new CampaignPage(campaignCustomList, campaignsPage.getTotalPages(),
                campaignsPage.getNumberOfElements());
    }

    @Override
    public List<LocalDate> getAllEndDates() {
        return this.campaignsRepository.findAllEndDates();
    }

    @Override
    public List<String> getStatusList() {
        List<String> statusList = new ArrayList<>();
        for (Status status : Status.values()) {
            statusList.add(status.toString());
        }
        return statusList;
    }

    @Override
    public Campaigns saveCampaign(CampaignsDto campaignsDto) {
        Campaigns campaign = this.mapCampaignsDtoToCampaigns(campaignsDto);
        this.setCampaignStatus(campaign, LocalDate.now());
        return this.campaignsRepository.save(campaign);
    }

    @Override
    public void updateStatusCompareToCurrentDate(LocalDate localDate) {
        List<Campaigns> campaignsList = campaignsRepository.findAll();
        campaignsList.forEach(campaign -> {
            this.setCampaignStatus(campaign, localDate);
        });
    }

    private void setCampaignStatus(Campaigns campaign, LocalDate localDate) {

        if (localDate.isBefore(campaign.getStartDate()))
            campaign.setStatus(String.valueOf(Status.PROCESSING));

        else if (localDate.isAfter(campaign.getEndDate())) {
            if (localDate.isAfter(campaign.getEndDate().plusMonths(1)))
                campaign.setStatus(String.valueOf(Status.ARCHIVED));
            else
                campaign.setStatus(String.valueOf(Status.PUBLISHED));
        } else
            campaign.setStatus(String.valueOf(Status.ONGOING));
    }

    private CampaignsDto mapCampaignsToCampaignsDto(Campaigns campaign) {
        CampaignsDto campaignsDto = new CampaignsDto();
        campaignsDto.setId(campaign.getId());
        campaignsDto.setName(campaign.getName());
        campaignsDto.setStatus(campaign.getStatus());
        campaignsDto.setStartDate(campaign.getStartDate());
        campaignsDto.setEndDate(campaign.getEndDate());
        return campaignsDto;
    }

    private Campaigns mapCampaignsDtoToCampaigns(CampaignsDto campaignsDto) {
        Campaigns campaign = new Campaigns();
        campaign.setId(campaignsDto.getId());
        campaign.setName(campaignsDto.getName());
        campaign.setStatus(campaignsDto.getStatus());
        campaign.setStartDate(campaignsDto.getStartDate());
        campaign.setEndDate(campaignsDto.getEndDate());

        return campaign;
    }

    private List<CampaignCustom> calculateDaysForAllCampaigns(List<CampaignsDto> campaignsDtoList) {
        List<CampaignCustom> campaignCustomList = new ArrayList<>();
        LocalDate localDate = LocalDate.now();

        campaignsDtoList.forEach(campaignsDto -> {
            CampaignCustom campaignCustom = this.mapCampaignDtoToCustom(campaignsDto);
            LocalDate startDate = campaignsDto.getStartDate();
            LocalDate endDate = campaignsDto.getEndDate();

            if (localDate.isBefore(startDate)) {
                long daysCount = Math.abs(DAYS.between(localDate, startDate));
                campaignCustom.setDaysCount(daysCount);
            } else if (localDate.isAfter(endDate))
                campaignCustom.setDaysCount(0);

            else {
                long daysCount = Math.abs(DAYS.between(endDate, localDate));
                campaignCustom.setDaysCount(daysCount);
            }


            campaignCustomList.add(campaignCustom);
        });

        return campaignCustomList;
    }

    private CampaignCustom mapCampaignDtoToCustom(CampaignsDto campaignsDto) {
        CampaignCustom campaignCustom = new CampaignCustom();
        LocalDate startDate = campaignsDto.getStartDate();
        LocalDate endDate = campaignsDto.getEndDate();
        campaignCustom.setId(campaignsDto.getId());
        campaignCustom.setName(campaignsDto.getName());
        campaignCustom.setStatus(campaignsDto.getStatus());
        campaignCustom.setStartDate(startDate);
        campaignCustom.setEndDate(endDate);
        campaignCustom.setDuration(startDate + " - " + endDate);

        return campaignCustom;
    }


    private List<CampaignsDto> mapCampaignsListToDtoList(List<Campaigns> campaignsList) {
        List<CampaignsDto> campaignsDtoList = new ArrayList<>();
        campaignsList.forEach(campaign -> {
            CampaignsDto campaignsDto = mapCampaignsToCampaignsDto(campaign);
            campaignsDtoList.add(campaignsDto);
        });
        return campaignsDtoList;
    }

    @SuppressWarnings("unchecked")
    private Page<Campaigns> findCampaignPage(CampaignFiltersDto campaignFiltersDto,
                                             Pageable pageable) {

        return campaignsRepository.findAll(new Specification<Campaigns>() {
            List<Predicate> predicates = new ArrayList<>();

            List<Predicate> endDatePredicates = new ArrayList<>();
            List<Predicate> statusPredicates = new ArrayList<>();

            @Override
            public Predicate toPredicate(Root<Campaigns> root, CriteriaQuery<?> criteriaQuery,
                                         CriteriaBuilder criteriaBuilder) {
                if (campaignFiltersDto != null) {

                    List<LocalDate> endDate = campaignFiltersDto.getEndDate();
                    if (endDate != null && endDate.size() != 0 && endDatePredicates.size() == 0) {
                        endDate.forEach(ed -> {
                            endDatePredicates.add(criteriaBuilder.equal(root.get("endDate"), ed));

                        });
                        predicates.add(criteriaBuilder.or(endDatePredicates.
                                toArray(new Predicate[endDatePredicates.size()])));
                    }

                    List<String> status = campaignFiltersDto.getStatus();
                    if (status != null && status.size() != 0 && statusPredicates.size() == 0) {
                        status.forEach(st -> {
                            statusPredicates.add(criteriaBuilder.equal(root.get("status"), st));
                        });

                        predicates.add(criteriaBuilder.or(statusPredicates.
                                toArray(new Predicate[statusPredicates.size()])));
                    }

                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
    }

}
