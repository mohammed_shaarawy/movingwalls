package com.movingwalls.mvc.business.domain.customs;

import com.movingwalls.mvc.business.domain.dtos.CampaignsDto;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Muhammad
 * @since 8/3/2020.
 */
public class CampaignCustom {
    private Long id;
    private String name;
    private String status;
    private LocalDate startDate;
    private LocalDate endDate;
    private String duration;
    private long daysCount;

    public CampaignCustom() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public long getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(long daysCount) {
        this.daysCount = daysCount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CampaignCustom)) return false;
        CampaignCustom that = (CampaignCustom) o;
        return getDaysCount() == that.getDaysCount() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getStatus(), that.getStatus()) &&
                Objects.equals(getStartDate(), that.getStartDate()) &&
                Objects.equals(getEndDate(), that.getEndDate()) &&
                Objects.equals(getDuration(), that.getDuration());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getStatus(), getStartDate(), getEndDate(), getDuration(), getDaysCount());
    }
}
