package com.movingwalls.mvc.business.domain.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Muhammad
 * @since 8/3/2020.
 */
public enum Status {
    PROCESSING, ONGOING, PUBLISHED, ARCHIVED;
}
