package com.movingwalls.mvc.business.domain.dtos;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Date;

/**
 * @author Muhammad
 * @since 7/31/2020.
 */

public class CampaignsDto {
    private Long id;
    @NotBlank
    private String name;
    private String status;
    @NotBlank
    private LocalDate startDate;
    @NotBlank
    private LocalDate endDate;

    public CampaignsDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
