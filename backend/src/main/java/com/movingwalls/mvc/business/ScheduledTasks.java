package com.movingwalls.mvc.business;

import com.movingwalls.mvc.business.service.interfaces.CampaignsServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Muhammad
 * @since 8/2/2020.
 */


@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    @Autowired
    private CampaignsServiceInterface campaignsServiceInterface;

    //run every 5 minutes.
    @Scheduled(cron = "5 * * * * *")
    public void updateStatusCampaignCron() {
        LocalDate localDate = LocalDate.now();
        campaignsServiceInterface.updateStatusCompareToCurrentDate(localDate);
    }

}
