package com.movingwalls.mvc.business.domain.dtos;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Muhammad
 * @since 8/6/2020.
 */
public class CampaignFiltersDto {

    private List<LocalDate> endDate;
    private List<String> status;

    public CampaignFiltersDto() {
    }

    public CampaignFiltersDto(List<LocalDate> endDate, List<String> status) {
        this.endDate = endDate;
        this.status = status;
    }


    public List<LocalDate> getEndDate() {
        return endDate;
    }

    public void setEndDate(List<LocalDate> endDate) {
        this.endDate = endDate;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

}
