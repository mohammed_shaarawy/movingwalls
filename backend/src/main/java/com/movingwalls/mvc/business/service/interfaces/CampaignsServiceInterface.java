package com.movingwalls.mvc.business.service.interfaces;

import com.movingwalls.mvc.business.domain.customs.CampaignCustom;
import com.movingwalls.mvc.business.domain.customs.CampaignPage;
import com.movingwalls.mvc.business.domain.dtos.CampaignFiltersDto;
import com.movingwalls.mvc.business.domain.dtos.CampaignsDto;
import com.movingwalls.mvc.dao.entities.Campaigns;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Muhammad
 * @since 7/31/2020.
 */

@Transactional
public interface CampaignsServiceInterface {
    /**
     * get all campaigns.
     *
     * @return list of campaigns dto.
     */
    List<CampaignsDto> getAllCampaigns();

    /**
     * get all the campaign custom objects from the DB.
     *
     * @param campaignFiltersDto dto for campaign filters
     * @param page               page number.
     * @param size               number of records.
     * @param sort               sort attributes.
     * @return campaign page.
     */
    CampaignPage getAllCampaignCustoms(CampaignFiltersDto campaignFiltersDto, Integer page, Integer size, Sort sort);

    /**
     * find all campaigns by search term.
     *
     * @param searchTerm term.
     * @param page       page number.
     * @param size       number of records.
     * @param sort       sort attributes.
     * @return campaign page.
     */
    CampaignPage findAllBySearchTerm(String searchTerm, Integer page, Integer size, Sort sort);

    /**
     * save campaign
     *
     * @param campaignsDto campaign dto.
     * @return result message.
     */
    Campaigns saveCampaign(CampaignsDto campaignsDto);

    /**
     * update the campaign status according to the current date.
     *
     * @param localDate current date.
     * @return true or false
     */
    void updateStatusCompareToCurrentDate(LocalDate localDate);


    /**
     * Get all the end dates for a lookup.
     *
     * @return All the universities names in unique values.
     */
    List<LocalDate> getAllEndDates();

    /**
     * get Status List.
     *
     * @return list of status
     */
    List<String> getStatusList();
}
