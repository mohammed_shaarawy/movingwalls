package com.movingwalls.mvc.business.domain.customs;

import java.util.List;

/**
 * @author Muhammad
 * @since 8/4/2020.
 */
public class CampaignPage {

    private List<CampaignCustom> campaignCustomList;
    private Integer totalPagesCount;
    private Integer totalRecordsCount;


    public CampaignPage() {
    }

    public CampaignPage(List<CampaignCustom> campaignCustomList, Integer totalPagesCount, Integer totalRecordsCount) {
        this.campaignCustomList = campaignCustomList;
        this.totalPagesCount = totalPagesCount;
        this.totalRecordsCount = totalRecordsCount;
    }

    public List<CampaignCustom> getCampaignCustomList() {
        return campaignCustomList;
    }

    public void setCampaignCustomList(List<CampaignCustom> campaignCustomList) {
        this.campaignCustomList = campaignCustomList;
    }

    public Integer getTotalPagesCount() {
        return totalPagesCount;
    }

    public void setTotalPagesCount(Integer totalPagesCount) {
        this.totalPagesCount = totalPagesCount;
    }

    public Integer getTotalRecordsCount() {
        return totalRecordsCount;
    }

    public void setTotalRecordsCount(Integer totalRecordsCount) {
        this.totalRecordsCount = totalRecordsCount;
    }
}
