package com.movingwalls.mvc.dao.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Muhammad
 * @since 7/31/2020.
 */

@Entity
@Table(name = "campaigns")
@NamedQuery(name = "Campaigns.findAll", query = "SELECT c FROM Campaigns c")
public class Campaigns implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "CAMPAIGNS_ID_GENERATOR")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAMPAIGNS_ID_GENERATOR")
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private String status;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;


    public Campaigns() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Campaigns)) return false;
        Campaigns campaigns = (Campaigns) o;
        return Objects.equals(getName(), campaigns.getName()) &&
                Objects.equals(getStatus(), campaigns.getStatus()) &&
                Objects.equals(getStartDate(), campaigns.getStartDate()) &&
                Objects.equals(getEndDate(), campaigns.getEndDate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getStatus(), getStartDate(), getEndDate());
    }
}
