package com.movingwalls.mvc.dao.repository;

import com.movingwalls.mvc.dao.entities.Campaigns;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Muhammad
 * @since 7/31/2020.
 */

@Repository
public interface CampaignsRepository extends JpaRepository<Campaigns, Long> {

    /**
     * Find all the end dates for the lookup.
     *
     * @return all the end dates.
     */
    @Query("select DISTINCT(c.endDate) from Campaigns c order by c.endDate desc")
    List<LocalDate> findAllEndDates();

    /**
     * find page of campaigns sorted, paginated and filtered.
     *
     * @param specification interface
     * @param pageable      interface
     * @return page of campaigns
     */
    Page<Campaigns> findAll(Specification<Campaigns> specification, Pageable pageable);


    /**
     * find campaigns by global search.
     *
     * @param searchTerm the search term.
     * @return list of campaigns.
     */
    @Query(value = "select c from Campaigns c " +
            " where c.name like %?1% or c.status like %?1%" +
            " or c.startDate like %?1% or c.endDate like %?1% ")
    Page<Campaigns> findAll(String searchTerm, Pageable pageable);
}
