package com.movingwalls.mvc.controllers;

import com.movingwalls.mvc.business.domain.customs.CampaignPage;
import com.movingwalls.mvc.business.domain.dtos.CampaignFiltersDto;
import com.movingwalls.mvc.business.domain.dtos.CampaignsDto;
import com.movingwalls.mvc.business.service.interfaces.CampaignsServiceInterface;
import com.movingwalls.mvc.dao.entities.Campaigns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Muhammad
 * @since 7/31/2020.
 */

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/campaigns")
public class CampaignsController {

    @Autowired
    private CampaignsServiceInterface campaignsServiceInterface;

    @GetMapping("/getAll")
    public List<CampaignsDto> getAllCampaigns() {
        return this.campaignsServiceInterface.getAllCampaigns();
    }

    @PostMapping("/getAllCustom")
    public CampaignPage getAllCampaignsCustom(@RequestBody CampaignFiltersDto campaignFiltersDto,
                                              @SortDefault(sort = {"name", "status"}, direction = Sort.Direction.ASC)
                                                      Sort sort,
                                              @RequestParam(defaultValue = "0") Integer page,
                                              @RequestParam(defaultValue = "20") Integer size) {
        return this.campaignsServiceInterface.getAllCampaignCustoms(campaignFiltersDto, page, size, sort);
    }

    @PostMapping("/getAllBySearchTerm")
    public CampaignPage getAllCampaignsBySearchTerm(@RequestBody String searchTerm,
                                              @SortDefault(sort = {"name", "status"}, direction = Sort.Direction.ASC)
                                                      Sort sort,
                                              @RequestParam(defaultValue = "0") Integer page,
                                              @RequestParam(defaultValue = "20") Integer size) {

        return this.campaignsServiceInterface.findAllBySearchTerm(searchTerm, page, size, sort);

    }


    @PostMapping("/saveCampaign")
    public Campaigns saveCampaign(@RequestBody CampaignsDto campaignsDto) {
        return this.campaignsServiceInterface.saveCampaign(campaignsDto);
    }

    @GetMapping("/getAllEndDates")
    public List<LocalDate> getAllEndDates() {
        return this.campaignsServiceInterface.getAllEndDates();
    }


    @GetMapping("/getStatusList")
    public List<String> getStatusList() {
        return this.campaignsServiceInterface.getStatusList();
    }


}
